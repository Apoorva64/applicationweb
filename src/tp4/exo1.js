// calcule le prix TTC à partir du prix hors-taxe
// et de la TVA
// affiche une alerte avec un message d'erreur si les
// valeurs fournies ne sont pas des nombres
function prixTTC() {
    let nonTVAPrice = parseFloat(document.getElementById("pht").value);
    let TVA = parseFloat(document.getElementById("tva").value);
    let priceWithTax = 0;
    if (isNaN(nonTVAPrice) || isNaN(TVA)) {
        alert("Veuillez entrer des nombres");
    } else {
        priceWithTax = nonTVAPrice + (nonTVAPrice * TVA / 100);
        document.getElementById("pttc").innerText = priceWithTax.toFixed(2);
        document.getElementById("resultat").style = "visibility:visible";

    }
}
