// étant donnée la moyenne 'm'
// retourne l'appréciation correspondante
// (une chaîne de caractères)
function appreciation(m) {
    let divString = "";
    if (m < 6) {
        divString = "très insuffisant";
    } else if (m < 10) {
        divString = "insuffisant";
    } else if (m < 13) {
        divString = "moyen";
    } else if (m < 16) {
        divString = "bien";
    } else if (m < 19) {
        divString = "très bien";
    } else {
        divString = "excellent";
    }
    document.getElementById("appreciation").innerHTML = divString;

}

// calcule la moyenne à partir des trois notes
// et affiche la mmoyenne et l'appréciation correspondante
function calculer() {
    let mean = (parseInt(document.getElementById("note1").value) +
        parseInt(document.getElementById("note2").value) +
        parseInt(document.getElementById("note3").value)) / 3;
    if (isNaN(mean)) {
        alert("Veuillez entrer des notes valides");
        document.getElementById("resultat").style = "visibility: hidden";
    }


    document.getElementById("moyenne").innerText = mean.toFixed(2).toString();
    appreciation(mean);
    document.getElementById("resultat").style = "visibility:visible";

}
