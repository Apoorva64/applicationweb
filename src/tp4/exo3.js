let x; // le premier nombre de la multiplication
let y; // le deuxième nombre de la multiplication

// donne le rôle du bouton :
// si 'verifier' est 'true' alors le prochain clic sur le bouton
// est destiné à vérifier la réponse de l'utilisateur, sinon,
// le clic est destiné à proposer une nouvelle multiplication
let verifier = true;

// génére une nouvelle multiplication, autrement dit :
// - génère deux entiers au hasard dans l'intervalle [1,9]
// - les affiche dans les bons éléments HTML
function nouvelle() {

    // génère deux entiers au hasard dans l'intervalle [1,9]
    x = Math.floor(Math.random() * 9) + 1;
    y = Math.floor(Math.random() * 9) + 1;

    // affiche les deux entiers dans les bons éléments HTML
    document.getElementById("nombre1").innerHTML = x;
    document.getElementById("nombre2").innerHTML = y;


}

// cette fonction est appelée quand l'utilisateur clique
// sur le bouton. La fonction a deux rôles alternatifs :
// - vérifier que la proposition de l'utilisateur est correcte
// - générer une nouvelle multiplication
// Cette alternance est gérée à l'aide de la variable 'verifier'
function valider() {
    // get the user's answer
    let answer = parseFloat( document.getElementById("proposition").value);
    console.log(x * y);
    console.log(answer);

    // check if the user's answer is correct
    if (answer === x * y) {
        // if it is, display a message and generate a new multiplication
        document.getElementById("resultat").innerHTML = "Bonne réponse !";
        document.getElementById("resultat").style = "visibility: visible";
    }
    else {
        document.getElementById("resultat").innerHTML = "Mauvaise réponse !";
        document.getElementById("resultat").style = "visibility: visible";
    }
    document.getElementById("bouton").value = !verifier ? "Vérifier" : "Continuer";
    verifier = !verifier;
    if (verifier) {
        nouvelle();
    }

}


