<?php
    $secret = rand(0, 100);

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>TP 1 - Exo 10</title>
        <meta name="author" content="Marc Gaetano">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/tp1.css">
    </head>
    <body>
        <h1>TP 1 - Exo 10</h1>
        <hr>

        <?php
            $html = "";
            if (!isset($_POST['essais']) or $_POST['essais'] == 0) {
                $html .= "<h3>Je pense à un nombre compris entre 1 et 100... à vous de le deviner !</h3>";
                $html .= "<form class='exo10' method='POST' action='exo12.php'>
  Votre proposition : <input type='text' name='proposition'>
  <input type='hidden' name='essais' value='1'>
  <input type='hidden' name='secret' value='$secret'>
  <input type='submit' value='Vérifier'>
</form>";
            } elseif ($_POST['secret'] > $_POST['proposition']) {
                $proposition = $_POST['proposition'];
                $secret = $_POST['secret'];
                $triesNext = $_POST['essais'] + 1;
                $html .= "<h2>Vous proposez $proposition :</h2>
                          <h3>Trop petit, essayez encore...</h3>";
                $html .= "<form class='exo10' method='POST' action='exo12.php'>
  Votre proposition : <input type='text' name='proposition'>
  <input type='hidden' name='essais' value='$triesNext'>
  <input type='hidden' name='secret' value='$secret'>
  <input type='submit' value='Vérifier'>
</form>";
            } elseif ($_POST['secret'] < $_POST['proposition']) {
                $proposition = $_POST['proposition'];
                $secret = $_POST['secret'];
                $triesNext = $_POST['essais'] + 1;
                $html .= "<h2>Vous proposez $proposition :</h2>
                          <h3>Trop grand, essayez encore...</h3>";
                $html .= "<form class='exo10' method='POST' action='exo12.php'>
  Votre proposition : <input type='text' name='proposition'>
  <input type='hidden' name='essais' value='$triesNext'>
  <input type='hidden' name='secret' value='$secret'>
  <input type='submit' value='Vérifier'>
</form>";
            } else {
                $proposition = $_POST['proposition'];
                $secret = $_POST['secret'];
                $tries = $_POST['essais'];
                $html .= "<h2>Vous proposez $proposition :</h2>";
                $html .= "<h3>Bravo, vous avez trouvé en $tries essai(s) !</h3>";
                $html.= '<a class="bouton" href="exo12.php">Autre partie</a>	</body>';
            }

            echo $html;
        ?>

    </body>
</html>
