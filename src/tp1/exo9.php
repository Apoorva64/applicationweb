<?php

    # retourne la chaîne '$s' normalisée
    # (toutes les lettres en minuscule sauf la première)
    function normalize($s)
    {
        $s = strtolower($s);
        return ucfirst($s);
    }


    # Teste si les prénom et nom sont bien renseignés et
    # retourne le tableau des messages d'erreurs
    # (tableau vide s'il n'y a pas d'erreur)
    function check_input()
    {
        $errors = array();

        if (empty($_POST['prenom'])) {
            $errors[] = 'Vous devez saisir votre prénom.';
        }
        if (empty($_POST['nom'])) {
            $errors[] = 'Vous devez saisir votre nom.';
        }

        return $errors;
    }

    # retourne le code HTML (une chaîne de caractères)
    # d'une liste "<ul><li>..</li>..</ul>", les
    # éléments de liste contenant les erreurs
    # contenues dans le tableau '$errors'
    function display_errors($errors)
    {
        $html = "<ul>";
        $html .= "<li>" . implode("</li><li>", $errors) . "</li>";
        $html .= "</ul>";
        return $html;
    }

    # retourne le code HTML (une chaîne de caractères)
    # d'un heading "<h2>...</h2>" contenant le message
    # de bienvenu en anglais
    function display_welcome($h, $c, $p, $n)
    {
        $html = "<h2>";
        if (date("h") < 12) {
            $html = "Good morning";
        } elseif (date("h") < 18) {
            $html = "Good afternoon";
        } else {
            $html = "Good evening";
        }
        $html .= "$h $c $p $n";
        $html .= "</h2>";
        return $html;
    }


?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>TP 1 - Exo 9</title>
        <meta name="author" content="Marc Gaetano">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/tp1.css">
    </head>
    <body>
        <h1>TP 1 - Exo 9</h1>
        <hr>
        <?php


            $errors = check_input();
            $prenom = $_POST['prenom'];
            $nom = $_POST['nom'];
            $civilite = $_POST['civilite'];

            # normalisation des données
            $prenom = normalize($prenom);
            $nom = normalize($nom);

            # si pas d'erreur
            if (empty($errors)) {
                # affichage du message de bienvenue
                echo display_welcome("", $civilite, $prenom, $nom);
            } else {
                # affichage des erreurs
                echo display_errors($errors);
            }

        ?>
    </body>
</html>
