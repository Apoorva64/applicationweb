<?php


    # retourne le code HTML (une chaîne de caractères)
    # d'une table '$n'x'$n' représentant un échiquier
    # alternant cases blanches et noires
    function table($n)
    {
        $html = "<table class='exo7'>";
        for ($i = 0; $i < $n; $i++) {
            $html .= "<tr>";
            for ($j = 0; $j < $n; $j++) {
                if (($i + $j) % 2 == 0) {
                    $html .= '<td class="blanc"></td>';
                } else {
                    $html .= '<td class="noir""></td>';
                }
            }
            $html .= "</tr>";
        }
        $html .= "</table>";
        return $html;
    }


?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>TP 1 - Exo 6</title>
        <meta name="author" content="Marc Gaetano">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/tp1.css">
    </head>
    <body>
        <h1>TP 1 - Exo 6</h1>
        <hr>
        <h2><?php
                if(isset($_GET['taille']))
                    $size=$_GET['taille'];
                else $size = 8;
                echo "Echiquier " . $size . "x" . $size ;
            ?></h2>
        <?php echo table($size) ?>


    </body>
</html>
