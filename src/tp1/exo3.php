<?php

    # retourne le code HTML (une chaîne de caractères)
    # d'une table contenant les diviseurs de '$N'
    # (une seule ligne, autant de colonnes que de diviseurs)
    function divisors($n)
    {
        $divisors = array();
        for ($i = 1; $i <= $n; $i++) {
            if ($n % $i == 0) {
                $divisors[] = $i;
            }
        }
        return $divisors;
    }


?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>TP 1 - Exo 3</title>
        <meta name="author" content="LP IMApp">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/tp1.css">
    </head>
    <body>
        <h1>TP 1 - Exo 3</h1>
        <hr>
        <h2> Les diviseurs de <?php $n = $_GET["n"] ; echo $n?> sont: </h2>
        <?php
            $html = "<table class='exo4'>";
            $html .= "<tr>";
            $html .= "<td>";
            $html .= implode("</td><td>", divisors($n));
            $html .= "</td>";
            $html .= "</tr>";
            $html .= "</table>";
            echo $html;
        ?>

    </body>
</html>
