<?php
    $randomNumber1 = rand(2, 10);
    $randomNumber2 = rand(2, 10);

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>TP 1 - Exo 11</title>
        <meta name="author" content="Marc Gaetano">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/tp1.css">
    </head>
    <body>
        <h1>TP 1 - Exo 11</h1>
        <hr>
        <form action="exo11-action.php" method="get">
            <?php
                echo '<input type="hidden" name="randomNumber1" value="' . $randomNumber1 . '">';
                echo '<input type="hidden" name="randomNumber2" value="' . $randomNumber2 . '">';
                $label = $randomNumber1 . " x " . $randomNumber2 . " =";
                echo ' <label for="result">' . $label . '</label>';
                echo '<input type="number" name="result" placeholder="?">';
            ?>
        </form>

    </body>
</html>
