<?php

    # retourne le nom du jour de la semaine
    # correspondant à '$week', le  numéro du
    # jour de la semaine (0 -> dimanche, 1 -> lundi, ...)
    function jour($week)
    {
        $jours = array("dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi");
        return $jours[$week];
    }

    $day = $_GET['jour'];
    $month = $_GET['mois'];
    $year = $_GET['annee'];

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>TP 1 - Exo 10</title>
        <meta name="author" content="Marc Gaetano">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/tp1.css">
    </head>
    <body>
        <h1>TP 1 - Exo 10</h1>
        <hr>
        <h2><?php
                $day_name = jour(date('w', mktime(0, 0, 0, $month, $day, $year)));
                echo "Le $day/$month/$year est un $day_name";
            ?></h2>
    </body>
</html>
