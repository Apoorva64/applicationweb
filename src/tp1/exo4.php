<?php

    function divisors($n)
    {
        $divisors = array();
        for ($i = 1; $i <= $n; $i++) {
            if ($n % $i == 0) {
                $divisors[] = $i;
            }
        }
        return $divisors;
    }

    # si '$N' est premier, retourne '$N'
    # sinon retourne le plus petit diviseur
    # de '$N'. Par exemple :
    # - premier(13) -> 13
    # - premier(35) -> 5
    function premier($N)
    {
        $divisors_array = divisors($N);
        if (count($divisors_array) == 3) return $divisors_array[1];
        else return $N;
    }

    # retourne une chaîne de caractères du type :
    # - "Le nombre N est premier" si '$N' est premier
    # - "Le nombre N n'est pas premier car multiple de D"
    #   si '$N' est divisible par un nombre D (et donc, pas premier)
    function resultat($N)
    {
        $divisors_array = divisors($N);
        if (count($divisors_array) < 3) return "Le nombre $N est premier !";
        else return "Le nombre $N n'est pas premier car il est divisible par " . $divisors_array[1]." !";
    }


?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>TP 1 - Exo 4</title>
        <meta name="author" content="Marc Gaetano">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" href="css/tp1.css">
    </head>
    <body>
        <h1>TP 1 - Exo 4</h1>
        <hr>
        <h2><?php echo resultat((int)$_GET['nombre']) ?></h2>
        <a class="bouton" href="exo4.html">Autre test</a>
    </body>
</html>
