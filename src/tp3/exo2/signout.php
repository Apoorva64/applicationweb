<?php
    // à compléter
    // dans cette partie, on détruit la session
    // et on redirige l'utilisateur vers la page de login

    // on détruit la session
    session_start();
    session_unset();
    session_destroy();
    // on redirige l'utilisateur vers la page de login
    header('Location: signin.php');
?>
