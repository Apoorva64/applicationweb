<?php
function divisors($n)
{
    $divisors = array();
    for ($i = 1; $i <= $n; $i++) {
        if ($n % $i == 0) {
            $divisors[] = $i;
        }
    }
    return $divisors;
}
print json_encode(divisors(60));