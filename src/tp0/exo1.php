<?php
    function factorial($n)
    {
        if ($n < 0) {
//        throw new \http\Exception\InvalidArgumentException::("cannot be a negative number");
            return 0;
        }
        if ($n < 1) {
            return 1;
        }
        $result = 1;
        for ($i = 2; $i < $n + 1; $i++) {
            $result = $result * $i;
        }
        return $result;
    }

    echo factorial(1);
