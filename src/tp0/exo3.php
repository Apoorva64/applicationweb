<?php
function median($tab)
{
    sort($tab);
    $size = count($tab);
    if ($size % 2 == 0)
        return ($tab[$size / 2 - 1] + $tab[$size / 2]) / 2;
    else
        return $tab[$size / 2];
}