<?php
function countOccurrences($str, $char)
{
    $count = 0;
    for ($i = 0; $i < strlen($str); $i++) {
        if ($str[$i] == $char) {
            $count++;
        }
    }
    return $count;
}

echo countOccurrences("bonjour", "o");
