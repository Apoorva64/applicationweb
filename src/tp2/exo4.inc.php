<?php

    // remplit les tableaux '$day', '$month' et '$lang'
    // à partir des informations contenues dans les fichiers
    // '*.csv' contenus dans le répertoire '$folderpath'
    function fillArrays($folderpath, &$day, &$month, &$lang)
    {
        $files = scandir($folderpath);
        foreach ($files as $file) {
            $filepath = $folderpath . '/' . $file;
            $handle = fopen($filepath, 'r');
            $langStr = trim(fgets($handle));
            if ($langStr) {
                $lang[] = $langStr;
                $days = fgetcsv($handle);
                $months = fgetcsv($handle);
                $day[$langStr] = $days;
                $month[$langStr] = $months;
            }
            fclose($handle);
        }
    }

    // pour comprendre ce que cette fonction doit générer
    // regardez le code source HTML du fichier exemple fourni
    function makeRadio($keyval, $name)
    {
        $html = "<div>";
        foreach ($keyval as $key => $value) {
            $html .=
                "<fieldset>
				    <input type='radio' name='$name' value='$value'> $value
			     </fieldset>";
        }
        $html .= "</div>";
        return $html;

    }

    // retourne une chaîne de caractères qui donne
    // la date dans la langue déterminée par le code '$langue'
    function makeDate($langue, $jour, $mois)
    {
        $dayName = $jour[$langue][date('w')];
        $monthName = $mois[$langue][(int)date('n') - 1];
        return "$dayName " . date('j') . " $monthName " . date('Y');
    }

    $LANGUE = [];
    $JOUR = [];
    $MOIS = [];

    fillArrays("exo4", $JOUR, $MOIS, $LANGUE);

