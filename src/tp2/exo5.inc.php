<?php

    // retiourne une chaîne de caractères identique
    // à '$nom' mais avec tous les caractères en
    // minuscule et avec la première lettre en majuscule
    function normalize($nom)
    {
        $nom = strtolower($nom);
        $nom = ucfirst($nom);
        return $nom;
    }

    // lit le fichier '$student_file' et retourne un tableau
    // associatif de la forme [ ID => [ PRENOM , NOM ], ... ]
    // où ID est l'identifiant, PRENOM le prénom et
    // NOM le nom de l'étudiant
    function student_array($student_file)
    {
        $tab = array();
        $f = fopen($student_file, "r");
        while (!feof($f)) {
            $line = fgetcsv($f, 0, ";");
            if (count($line) == 3) {
                $tab[$line[0]] = array_slice($line, 1);
            }
        }
        fclose($f);
        return $tab;
    }

    // lit le fichier '$score_file' et retourne un tableau
    // associatif de la forme [ ID => [ NOTE1, NOTE2, NOTE3 ], ... ]
    // où ID est l'identifiant, et NOTE1, NOTE2 et NOTE3 les trois
    // notes obtenues par l'étudiant
    function score_array($score_file)
    {
        $tab = array();
        $f = fopen($score_file, "r");
        while (!feof($f)) {
            $line = fgetcsv($f, 0, ";");
            if (count($line) == 4) {
                $tab[$line[0]] = array_slice($line, 1);
            }
        }
        fclose($f);
        return $tab;
    }

    // retourne la moyenne des valeurs
    // contenues dans le tableau '$tabnotes'
    function average($tabnotes)
    {
        $somme = 0;
        foreach ($tabnotes as $note) {
            $somme += $note;
        }
        return $somme / count($tabnotes);
    }

    // retourne le TR adéquat qui contient successivement dans
    // les sept TD successifs l'identifiant, le prénom, le nom,
    // les trois notes et la moyenne de ces notes
    function student_score($id, $info, $score)
    {
        $moyenne = average($score);
        $tr = "<tr>";
        $tr .= "<td>$id</td>";
        $tr .= "<td>$info[0]</td>";
        $tr .= "<td>$info[1]</td>";
        foreach ($score as $note) {
            $tr .= "<td>$note</td>";
        }
        $tr .= "<td>$moyenne</td>";
        $tr .= "</tr>";
        return $tr;
    }

    // retourne les TR adéquats qui contiennent successivement
    // les informations des étudiants sélectionnés suivant la
    // valeur de '$name' :
    // - si '$name' est le prénom de l'étudiant, l'étudiant est sélectionné
    // - si '$name' est le nom de l'étudiant, l'étudiant est sélectionné
    // - si '$name' est la chaîne vide, l'étudiant est sélectionné
    function table_content($name, $students, $scores)
    {
        $tab = array();
        foreach ($students as $id => $info) {
            if ($name == $info[0] || $name == $info[1] || $name == "") {
                $tab[] = student_score($id, $info, $scores[$id]);
            }
        }
        return implode($tab);


    }

    $STUDENT_FILE = "exo5-6/students.csv";
    $SCORE_FILE = "exo5-6/scores.csv";

?>
