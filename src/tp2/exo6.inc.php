<?php
    // retiourne une chaîne de caractères identique
    // à '$nom' mais avec tous les caractères en
    // minuscule et avec la première lettre en majuscule
    function normalize($nom)
    {
        $nom = strtolower($nom);
        $nom = ucfirst($nom);
        return $nom;
    }

    // lit le fichier '$student_file' et retourne un tableau
    // associatif de la forme [ ID => [ PRENOM , NOM ], ... ]
    // où ID est l'identifiant, PRENOM le prénom et
    // NOM le nom de l'étudiant
    function student_array($student_file)
    {
        $tab = array();
        $f = fopen($student_file, "r");
        while (!feof($f)) {
            $line = fgetcsv($f, 0, ";");
            if (count($line) == 3) {
                $tab[$line[0]] = array_slice($line, 1);
            }
        }
        fclose($f);
        return $tab;
    }

    // lit le fichier '$score_file' et retourne un tableau
    // associatif de la forme [ ID => [ NOTE1, NOTE2, NOTE3 ], ... ]
    // où ID est l'identifiant, et NOTE1, NOTE2 et NOTE3 les trois
    // notes obtenues par l'étudiant
    function score_array($score_file)
    {
        $tab = array();
        $f = fopen($score_file, "r");
        while (!feof($f)) {
            $line = fgetcsv($f, 0, ";");
            if (count($line) == 4) {
                $tab[$line[0]] = array_slice($line, 1);
            }
        }
        fclose($f);
        return $tab;
    }

    // retourne la moyenne des valeurs
    // contenues dans le tableau '$tabnotes'
    function average($tabnotes)
    {
        $somme = 0;
        foreach ($tabnotes as $note) {
            $somme += (int)$note;
        }
        return $somme / count($tabnotes);
    }

    // retourne le TR adéquat qui contient successivement dans
    // les sept TD successifs l'identifiant, le prénom, le nom,
    // les trois notes et la moyenne de ces notes
    function student_score($id, $info, $score)
    {
        $moyenne = average($score);
        $tr = "<tr>";
        $tr .= "<td>$id</td>";
        $tr .= "<td>$info[0]</td>";
        $tr .= "<td>$info[1]</td>";
        foreach ($score as $note) {
            $tr .= "<td>$note</td>";
        }
        $tr .= "<td>$moyenne</td>";
        $tr .= "<td><a href='exo6-mod-formulaire.php?id=$id'>Modifier</a></td>";
        $tr .= "</tr>";
        return $tr;
    }

    // retourne les TR adéquats qui contiennent successivement
    // les informations des étudiants sélectionnés suivant la
    // valeur de '$name' :
    // - si '$name' est le prénom de l'étudiant, l'étudiant est sélectionné
    // - si '$name' est le nom de l'étudiant, l'étudiant est sélectionné
    // - si '$name' est la chaîne vide, l'étudiant est sélectionné
    function table_content($name, $students, $scores)
    {
        $tab = array();
        foreach ($students as $id => $info) {
            if ($name == $info[0] || $name == $info[1] || $name == "") {
                if ((string)$id){

                    $tab[] = student_score($id, $info, $scores[$id]);
                }
            }
        }
        return implode($tab);


    }

    // retourne le contenu de l'élément HTML FORM
    // pour comprendre ce que cette fonction doit générer
    // regardez le code source HTML du fichier exemple fourni
    function form_content($id, $students, $scores)
    {

        $name = $students[$id][0];
        $firstname = $students[$id][1];
        $score = $scores[$id];
        $html = "<td>$id</td>
				<td>
					<input type='text' name='prenom' value='$name'>
				</td>
				<td>
					<input type='text' name='nom' value='$firstname'>
				</td>
				<td>
					<input type='text' name='score1' value='$score[0]'>
				</td>
				<td>
					<input type='text' name='score2' value='$score[1]'>
				</td>
				<td>
					<input type='text' name='score3' value='$score[2]'>
				</td>
				<td>14.17</td>
				<td>
					<input type='submit' value='Valider'>
				</td>
				<input type='hidden' name='id' value='$id'>";
        return $html;
    }

    // sauve le tableau associatif '$array' dans le
    // fichier '$file' au format CSV. Le tableau est de
    // la forme [ ID => INFO ] où INFO est un tableau de
    // valeurs (associatif ou pas)
    function save_array($array, $file)
    {
        $f = fopen($file, "w");
        foreach ($array as $id => $info) {
            $line = "$id";
            foreach ($info as $value) {
                $line .= ";$value";
            }
            $line .= "\n";
            fwrite($f, $line);
        }
        fclose($f);
    }

    // modifie le contenu du tableau '$students' en associant les
    // valeurs '$firstnme' et '$lastname' aux clefs 'prenom' et 'nom'
    // pour la clef '$id'
    function update_student_array(&$students, $id, $firstname, $lastname)
    {
        $students[$id][0] = $firstname;
        $students[$id][1] = $lastname;
    }

    // modifie le contenu du tableau '$scores' en associant les
    // valeurs '$score1', '$score2' et '$score3' à la clef '$id'
    function update_score_array(&$scores, $id, $score1, $score2, $score3)
    {
        $scores[$id] = array($score1, $score2, $score3);
    }

    $STUDENT_FILE = "exo5-6/students.csv";
    $SCORE_FILE = "exo5-6/scores.csv";
    $SCORE_ARRAY = score_array($SCORE_FILE);
    $STUDENT_ARRAY = student_array($STUDENT_FILE);
