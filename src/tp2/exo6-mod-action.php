<?php
    include("exo6.inc.php");

    // à compléter

    // on récupère les données du formulaire
    $id = $_POST['id'];
    $name = $_POST['nom'];
    $firstname = $_POST['prenom'];
    $score1 = $_POST['score1'];
    $score2 = $_POST['score2'];
    $score3 = $_POST['score3'];
    update_score_array($SCORE_ARRAY, $id, $score1, $score2, $score3);
    update_student_array($STUDENT_ARRAY, $id, $name, $firstname);
    save_array($STUDENT_ARRAY, $STUDENT_FILE);
    save_array($SCORE_ARRAY, $SCORE_FILE);


?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>TP 2 - Exo 6</title>
        <meta name="author" content="Marc Gaetano">
        <meta name="viewport" content="width=device-width; initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/tp2.css">
    </head>
    <body>
        <h1>TP 2 - Exo 6</h1>
        <hr>

        <h2>Modification(s) effectuée(s)</h2>
        <a class="bouton" href="exo6-formulaire.html">Nouvelle recherche</a>
    </body>
</html>
