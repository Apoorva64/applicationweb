<?php

    // pour comprendre ce que cette fonction doit générer
    // regardez le code source HTML du fichier exemple fourni
    function makeRadio($info, $name)
    {
        $html = "<div>";
        foreach ($info as $key => $value) {
            $html .=
                "<fieldset>
				    <input type='radio' name='$name' value='$key'> $value[0]
			     </fieldset>";
        }
        $html .= "</div>";
        return $html;
    }

    // retourne le nom du pays de clef '$key'
    // '$key' est la clef dp nt la valeur est
    // l'information dans le tableau associatif '$info'
    function getCountryName($key, $info)
    {
        return $info[$key][0];
    }

    // retourne le nom de la capitale de clef '$key'
    // '$key' est la clef dp nt la valeur est
    // l'information dans le tableau associatif '$info'
    function getCapitalName($key, $info)
    {
        return $info[$key][2];

    }

    // retourne l'élément HTML IMG qui est l'image
    // du pays de clef '$key'
    // '$key' est la clef dp nt la valeur est
    // l'information dans le tableau associatif '$info'
    function getCountryImage($key, $info)
    {
        $img = $info[$key][1];
        return "<img class='exo2-3' src='exo2-3/$img'/>";


    }

    // retourne l'élément HTML IMG qui est l'image
    // de la capitale de clef '$key'
    // '$key' est la clef dp nt la valeur est
    // l'information dans le tableau associatif '$info'
    function getCapitalImage($key, $info)
    {
        $img = $info[$key][3];
        return "<img class='exo2-3' src='exo2-3/$img'/>";

    }

    $INFO = array();
    // open csv file
    $file = fopen("exo2-3/data.csv", "r");
    // read csv file
    while (!feof($file)) {
        $line = fgetcsv($file);
        if($line){
            $INFO[$line[0]] = $line;
        }
    }
    // close csv file
    fclose($file);
