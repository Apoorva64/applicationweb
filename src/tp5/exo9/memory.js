// le tableau qui contient le chemin
// du fichier image pour chaque image
var array = [];

// si clicked[i] == true alors array[i] est visible
var clicked = [];

// pour décider si un clic est
// un premier clic ou non
var first_click = true;

// l'indice de la première image cliquée
var first_index = 0;

// le nombre total de paires de clics
var clicks_number = 0;

// the nombre de paires de clics réussis
// (les paires de clics qui ont découvert
// des images identiques)
var good_clicks_number = 0;

// affecte à l'attribut src des deux images d'indice i et j
// le source de l'image "point d'interrogation"
function hide(i, j) {
    let images = document.getElementsByTagName("img");
    images[i].src = "images/question-mark.png";
    images[j].src = "images/question-mark.png";
}

// gère le clic sur l'image d'indice n
function click_image(n) {
    if (clicked[n]) {
        return;
    }
    clicked[n] = true;
    let images = document.getElementsByTagName("img");
    images[n].src = array[n];
    clicks_number++;
    if (first_click) {
        first_index = n;
        first_click = false;
    } else {
        if (array[first_index] === array[n]) {
            good_clicks_number++;
            first_click = true;
        } else {
            setTimeout(function () {
                hide(first_index, n);
                clicked[first_index] = false;
                clicked[n] = false;
                first_click = true;
            }, 1000);
        }
    }
    if (good_clicks_number === 8) {
        document.getElementById("result").innerHTML = "Bravo ! Vous avez gagné !";
        document.getElementById("result").style.visibility = "visible";
    }
}

// rempli le tableau array avec la valeur de
// l'attribut 'name' des images
function init() {
    let images = document.getElementsByTagName("img");
    for (let i = 0; i < images.length; i++) {
        array[i] = images[i].name;
    }
}

window.onload = init;