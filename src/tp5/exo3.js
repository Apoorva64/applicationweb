window.onload = function () {

    // affiche la source de l'image cliquée dans l'image
    // d'id "realize"
    function show(event) {
        let img = document.getElementById("realsize");
        img.src = event.target.src;
        document.getElementById("legend").innerHTML = event.target.title;
    }

    // ici, il faut relier la fonction "show" à l'évènement "onmouseover"
    // sur toutes les images ayant la classe "miniature"

    let images = document.getElementsByClassName("miniature");
    for (let i = 0; i < images.length; i++) {
        images[i].addEventListener("mouseover", show);
    }
};
