
window.onload = function () {

	// affiche le nombre "t" dans le span "spanElt"
	// "t" a au plus deux chiffres
	function afficher(t, spanElt) {
		let stringNum = t.toString();
		let images = Array.from( spanElt.children);
		if (stringNum.length === 1) {
			images[0].src = "images/0.png";
			images[1].src = "images/" + stringNum + ".png";
		} else {
			images[0].src = "images/" + stringNum[0] + ".png";
			images[1].src = "images/" + stringNum[1] + ".png";
		}
	}

	// met à jour les images de l'horloge
	// à chaque seconde
	function tictac() {
		let date = new Date();

		let seconds = date.getSeconds();
		afficher(seconds, document.querySelector("#horloge > span:nth-child(5)"));
		let minutes = date.getMinutes();
		afficher(minutes, document.querySelector("#horloge > span:nth-child(3)"));
		let hours = date.getHours();
		afficher(hours, document.querySelector("#horloge > span:nth-child(1)"));


	}

	// ici, il faut lancer l'horloge
	setInterval(tictac, 1);

};
