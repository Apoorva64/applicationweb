
window.onload = function () {

	// le "handler" du setTimeout
	let chrono = null;

	// si 'ok' est 'true', alors l'utilisateur
	// a choisi la bonne réponse
	let ok = false;

	// affiche le message 'm' avec la couleur 'c'
	// dans l'élément prévu à cet effet
	function msg(m, c) {
		document.getElementById('message').innerHTML = m;
		document.getElementById('message').style.color = c;
	}

	// cette fonction est appelée à l'issue
	// du setTimeout
	function stop() {
		// si l'utilisateur a choisi la bonne réponse
		if (ok) {
			// affiche le message 'Bravo !'
			msg('Bravo !', 'green');
		} else {
			// sinon affiche le message 'Perdu !'
			msg('Perdu !', 'red');
		}
	}

	// traite le "clic" sur un bouton radio
	function verifier(event) {
		// si l'utilisateur a choisi la bonne réponse
		console.log(event)
		if (event.target.hasAttribute('data-ok')) {
			ok = true;
			clearTimeout(chrono);
			msg('Bravo !', 'green');
			for (let i = 0; i < radios.length; i++) {
				radios[i].disabled = true;
				console.log('disabled')
			}

		} else {
			ok = false;
			msg('Perdu ! réessayez', 'red');
		}
		
	}

	// ici, on lance le setTimeout et stocke
	// le "handler" dans la variable 'chrono'
	chrono = setTimeout(stop, 5000);
	let radios = document.querySelector("body > p:nth-child(5)").querySelectorAll("input[type=radio]");
	for (let i = 0; i < radios.length; i++) {
		radios[i].addEventListener('click', verifier);
	}

	
};
