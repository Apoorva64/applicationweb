window.onload = function () {

    // pour distinguer les premiers clics
    // des seconds clics
    let first_click = true;

    // pour stocker la première image cliquée
    let first_image;

    // si "not_finished" est vrai, alors
    // il reste des images à permuter
    let not_finished = true;

    // traîte le clic sur une image
    function click_on(event) {

        if (first_click) {
            first_image = event.target;
            first_click = false;
        } else {
            let second_image = event.target;
            console.log(first_image);
            console.log(second_image);
            first_click = true;
            // swap the 2 images and image names
            let tmp = first_image.src;
            first_image.src = second_image.src;
            second_image.src = tmp;
            let tmp_name = first_image.name;
            first_image.name = second_image.name;
            second_image.name = tmp_name;
            // check if the game is finished
            is_finished();
        }

    }

    // teste si le puzzle est terminé
    function is_finished() {
        let imageContainer = document.getElementById("puzzle");
        let images = imageContainer.getElementsByTagName("img");

        let imageOrder = [];
        for (let i = 0; i < images.length; i++) {
            imageOrder.push(images[i].name);
        }
        let correctOrder = JSON.parse(JSON.stringify(imageOrder));
        correctOrder.sort();
        if (imageOrder.toString() === correctOrder.toString()) {
            document.getElementById("result").style.visibility = "visible";
            not_finished = false;
            for (let i = 0; i < images.length; i++) {
                images[i].onclick = null;
            }
        }

    }

    // ici, il faut relier la fonction "clic_on" à l'évènement "onclick"
    // sur toutes les images contenues dans l'élément d'id "puzzle"
    let puzzle = document.getElementById("puzzle");
    let images = puzzle.getElementsByTagName("img");
    for (let i = 0; i < images.length; i++) {
        images[i].onclick = click_on;
    }

};
