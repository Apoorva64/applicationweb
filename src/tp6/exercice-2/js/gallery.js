window.onload = function () {
    let id = 0;
    // l'élément d'id 'info' qui contient les
    // informations pour un personnage donné
    let info = document.querySelector("#info");

    // cette fonction place les données regroupées
    // dans le JSON 'data' dans les éléments adéquats, ainsi
    // que le 'src' comme valeur de l'attribut 'src' de
    // l'image adéquate
    function update(data, src) {
        console.log(data.responseText);

        data = JSON.parse(data.responseText);
        info.querySelector("#nom").innerHTML = data.nom;
        info.querySelector("#prenom").innerHTML = data.prenom;
        info.querySelector("#age").innerHTML = data.age;
        info.querySelector("#sexe").innerHTML = data.sexe;
        info.querySelector("#activite").innerHTML = data.activite;
        document.querySelector("#info > img").src = document.querySelector("#" + id).src;

    }

    // cette fonction est appelée lorsqu'on clique sur une image.
    // Elle récupère la valeur de l'attribut 'id' et effectue une
    // requête AJAX au script 'info.php' avec cette valeur en paramètre.
    // Elle mets à jour le contenu des éléments adéquat avec les valeurs
    // retournées par le script.
    function showinfo() {
        // récupération de la valeur de l'attribut 'id'
        document.querySelector("#info").style.visibility = 'visible';
        id = this.getAttribute("id");
        // on effectue une requête AJAX au script 'info.php'
        simpleAjax('info.php', "GET", 'id=' + id, update, null);
    }

    // ici, on ajoute l'évènement 'onclick' sur toutes les images
    // et on lie la fonction 'showInfo' à cet évènement
    let images = document.querySelectorAll("img");
    for (let i = 0; i < images.length; i++) {
        images[i].onclick = showinfo;
    }
    // ici, on ajoute l'évènement 'onclick' sur l'élément
    // d'id 'info' et on lie à cet évènement la fonction
    // qui cache cet élément
    document.querySelector("#info").onclick = function () {
        document.querySelector("#info").style.visibility = "hidden";
    }

};
