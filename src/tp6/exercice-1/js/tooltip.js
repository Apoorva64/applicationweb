window.onload = function () {

    // place un message d'erreur comme contenu de l'élément
    // d'id 'tooltip' et rend cet élément visible
    function on_failure(request) {
        document.querySelector("#tooltip").innerHTML = request.responseText;
        document.getElementById('tooltip').style.visibility = 'visible';
    }

    // place la réponse du serveur (request.responseText)
    // comme contenu de l'élément d'id 'tooltip' et rend
    // cet élément visible
    function on_success(request) {
        document.querySelector("#tooltip").innerHTML = request.responseText;
        document.querySelector("#tooltip").style.visibility = 'visible';
    }

    // supprime le contenu de l'élément d'id 'tooltip'
    // et rend cet élément caché
    function tooltip_hide() {
        document.querySelector("#tooltip").innerHTML = '';
        document.querySelector("#tooltip").style.visibility = 'hidden';

    }

    // effectue la requête Ajax sur le script 'dico.php'
    // avec, comme paramètre 'word', le mot sélectionné
    // sur le double-clic et :
    //   * appelle la fonction 'on_success' en cas de succès
    //   * appelle la fonction 'on_failure' en cas d'échec
    function tooltip_show() {
        // get the body element
        let body = document.querySelector("body");
        // get the selected word
        let word = document.getSelection().toString();
        console.log(word);
        simpleAjax('dico.php', "GET", 'word=' + word, on_success, on_failure);
    }

    // ici, il faut créer un nouvel élément 'div' avec
    // l'attribut 'id' ayant pour valeur 'tooltip', et
    // avec l'évènement 'onclick' lié à la fonction
    // 'tooltip_hide', et il faut ajouter ce nouvel élément
    // comme dernier fils de l'élément 'body'

    let body = document.querySelector('body');
    let div = document.createElement('div');
    div.id = 'tooltip';
    div.onclick = tooltip_hide;
    body.appendChild(div);


    // ici, il faut ajouter l'évènement 'ondblclick' sur
    // l'élément 'body' et le ier à la fonction 'tooltip_show'
    body.ondblclick = tooltip_show;


};
